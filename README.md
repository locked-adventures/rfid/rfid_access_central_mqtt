# RFID Access Central MQTT

Read cards from four RFID readers and publish for every reader, whether a card is present and whether it is the correct card.

The configuration can be set in a custom `config.cpp` file. It should define all the parameters which are declared in `config.h`.

Information is published on the following MQTT topics (`clientID` is specified in `config.cpp`):
- `<clientID>/status`: ONLINE or OFFLINE

For every RFID reader with n in {0, 1, 2, 3}:
- `<clientID>/get/rfid/<n>/present`: 1 if a card is present, 0 otherwise
- `<clientID>/get/rfid/<n>/correct`: 1 if a card with a correct ID is tapped, 0 otherwise
