#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>
#include <Arduino.h>

// Signature of the Ethernet begin method (in Arduino AVR Boards version 1.8.5):
// static int begin(uint8_t *mac, unsigned long timeout = 60000, unsigned long responseTimeout = 4000);
// Why mac is not const? No idea XD
extern uint8_t mac[];

extern const char brokerAddress[];
extern const int brokerPort;
extern const char clientID[];

class SingleRFIDReader {
  uint8_t pinCardPresent;
  uint8_t pinCardCorrect;

public:
  SingleRFIDReader()
  {}

  SingleRFIDReader(uint8_t pinCardPresent, uint8_t pinCardCorrect)
    : pinCardPresent(pinCardPresent)
    , pinCardCorrect(pinCardCorrect)
  {}

  void init() {
    pinMode(pinCardPresent, INPUT_PULLUP);
    pinMode(pinCardCorrect, INPUT_PULLUP);
  }

  bool isCardPresent() {
    return digitalRead(pinCardPresent) == LOW;
  }

  bool isCardCorrect() {
    return digitalRead(pinCardCorrect) == LOW;
  }
};

#define N_RFID_READERS 4

extern SingleRFIDReader rfidReaders[N_RFID_READERS];

#endif
