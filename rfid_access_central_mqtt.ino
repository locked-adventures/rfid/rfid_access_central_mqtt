#include <Ethernet.h>
#include <SPI.h>
// #include <WiFi101.h>
#include <MQTT.h>
#include "config.h"

#define BAUD_RATE 57600

static EthernetClient ethernetClient;
static MQTTClient mqttClient;

static void setupEthernet() {
  // Obtain IP address dynamically using DHCP
  Serial.println("Obtaining IP Address...");
  while (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to obtaining an IP address");

    // check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware)
      Serial.println("Ethernet shield was not found");

    // check for Ethernet cable
    if (Ethernet.linkStatus() == LinkOFF)
      Serial.println("Ethernet cable is not connected.");

    delay(1000);
  }

  Serial.print("Arduino's IP Address: ");
  Serial.println(Ethernet.localIP());

  Serial.print("DNS Server's IP Address: ");
  Serial.println(Ethernet.dnsServerIP());

  Serial.print("Gateway's IP Address: ");
  Serial.println(Ethernet.gatewayIP());

  Serial.print("Network's Subnet Mask: ");
  Serial.println(Ethernet.subnetMask());
}

static void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);

  // Note: Do not use the client in the callback to publish, subscribe or
  // unsubscribe as it may cause deadlocks when other things arrive while
  // sending and receiving acknowledgments. Instead, change a global variable,
  // or push to a queue and handle it in the loop after calling `client.loop()`.
}

typedef struct {
  int isPresent;
  int isCorrect;
} RfidState;

static RfidState rfidStateCache[N_RFID_READERS];

static void checkRfidStates(bool init = false) {
  for (int i = 0; i < N_RFID_READERS; i++) {
    bool isPresent = rfidReaders[i].isCardPresent();
    bool isCorrect = rfidReaders[i].isCardCorrect();

    if (init || rfidStateCache[i].isPresent != isPresent) {
      Serial.print("Card ");
      Serial.print(i);
      Serial.print(" present: ");
      Serial.println(isPresent ? "yes" : "no");
      mqttClient.publish(String(clientID) + "/get/rfid/" + i + "/present", isPresent ? "1" : "0", true, 0);
    }
    rfidStateCache[i].isPresent = isPresent;

    if (init || rfidStateCache[i].isCorrect != isCorrect) {
      Serial.print("Card ");
      Serial.print(i);
      Serial.print(" correct: ");
      Serial.println(isCorrect ? "yes" : "no");
      mqttClient.publish(String(clientID) + "/get/rfid/" + i + "/correct", isCorrect ? "1" : "0", true, 0);
    }
    rfidStateCache[i].isCorrect = isCorrect;
  }
}

static void connectMqtt() {
  Serial.print("Connecting to MQTT broker...");
  while (!mqttClient.connect(clientID)) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" Done.");
  checkRfidStates(true);
  mqttClient.publish((String(clientID) + "/status").c_str(), "ONLINE", true, 0);
}

static void setupMqtt() {
  mqttClient.begin(brokerAddress, brokerPort, ethernetClient);
  mqttClient.onMessage(messageReceived);
  mqttClient.setWill((String(clientID) + "/status").c_str(), "OFFLINE", true, 0);
}

static void initRfidReaders() {
  for (int i = 0; i < N_RFID_READERS; i++) {
    rfidReaders[i].init();
  }
}

void setup() {
  Serial.begin(BAUD_RATE);
  while (!Serial)
    ;
  Serial.println("Start of MQTT demo program");
  setupEthernet();
  setupMqtt();
  initRfidReaders();
  connectMqtt();
}

void loop() {
  Ethernet.maintain();
  mqttClient.loop();
  if (!mqttClient.connected()) {
    Serial.println("Mqtt client disconnected.");
    connectMqtt();
  }
  checkRfidStates();
  delay(10);
}
